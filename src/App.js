import React from 'react';
import './App.css';
import Home from './components/main/Home'
import ProductView from './components/product/ProductView'

function App() {
  return (
    <div>
      {/* <Home /> */}
      <ProductView />
    </div>
  );
}

export default App;
