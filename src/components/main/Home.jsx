import React, { Component } from 'react'
import DataService from '../../util/DataService'

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            datas: []
        }
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Name: <input type="text" value={this.state.value} onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Submit" />
                </form>
                <div className="container">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.datas.map(data =>
                                <tr key={data.id}>
                                    <td>{data.title}</td>
                                    <td>{data.finalPrice}</td>
                                    <td>{data.mrp}</td>
                                    <td>{data.discoutAmount}</td>
                                    <td>{data.discountPercentage}</td>
                                    <td>{data.companyName}</td>
                                    <td>{data.userRating}</td>
                                    <td>{data.baseRating}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }
    handleSubmit = (event) => {
        event.preventDefault();
        DataService.searchMobile(this.state.value)
            .then(response => this.setState({
                datas: response.data
            }))
    }
}

export default Home