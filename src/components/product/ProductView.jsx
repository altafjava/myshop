import React, { Component } from 'react'
import ProductItem from './ProductItem'
import './ProductView.css'
import './ProductItem.css'
import Search from './Search'
import DataService from '../../util/DataService'


class ProductView extends Component {
    render() {
        return (
            <>
                <main className="content">
                    <section className="product-list-wrapper">
                        <div className="product-list-header">
                            <Search />
                        </div>
                        <div className="product-list">
                            <ProductItem />
                            <ProductItem />
                            <ProductItem />
                            <ProductItem />
                        </div>
                    </section>
                </main>
            </>
        )
    }
}

export default ProductView
