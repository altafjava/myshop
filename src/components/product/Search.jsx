import React, { Component } from 'react'
import './ProductItem.css'
import './ProductView.css'
import './Button.css'
import './Icon.css'
import './SearchBar.css'
import DataService from '../../util/DataService'

class Search extends Component {
    constructor(props) {
        super(props)
        this.state = {
            searchString: '',
            datas: []
        }
    }
    render() {
        return (
            <div>
                <div>
                    <div className="product-list-header-title">
                        <h3>Eyewear</h3>
                    </div>
                    <div className="product-list-header-actions">
                        &nbsp;
                        <div className="filters-toggle">
                            <button className="button button-small button-border button-border-gray"  >Filters
                            <div className="filters-toggle-caret icon-caret" />
                            </button>
                        </div>&nbsp;
                        <div className="searchbar">
                            <input className="search-input react-tel-input searchbar-input"
                                onChange={this.onSearchChange}
                                onKeyPress={this.handleKeyPress}
                                placeholder="Filter products by keyword"
                                type="text"
                            />
                            <div className="searchbar-icon icon-magnify product-search-button" style={{ opacity: 1 }} />
                        </div>
                        &nbsp;
                        <div>
                            <button className="button button-small button-border button-border-gray"
                                onClick={this.onSearchClick}>Search</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    onSearchChange = (e) => {
        this.setState({
            searchString: e.target.value
        })
    }
    onSearchClick = (e) => {
        DataService.searchMobile(this.state.searchString)
            .then(response => this.setState({
                datas: response.data
            }))
    }
    handleKeyPress = (target) => {
        if (target.charCode == 13) {
            this.onSearchClick();
        }
    }
}

export default Search
