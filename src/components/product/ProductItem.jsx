import React from 'react'
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import ImageLoader from './ui/ImageLoader'
import PropTypes from 'prop-types';
import './ProductItem.css'

const ProductItem = ({
    product,
    onOpenModal,
    onClickProduct,
    dispatch,
    foundOnBasket
}) => {
    return (
        <SkeletonTheme color="#e1e1e1" highlightColor="#f2f2f2">
            <div className='product-card'>
                <div className="product-card-content">
                    <div className="product-card-img-wrapper">
                        {'product.image' ? (
                            <ImageLoader
                                className="product-card-img"
                                src='https://m.media-amazon.com/images/I/612+ULaEhDL._AC_UY218_ML3_.jpg'
                            />
                        ) : <Skeleton width={100} height={70} />}
                    </div>
                    <h5 className="product-card-name text-overflow-ellipsis margin-auto">{'Milo Everyday' || <Skeleton width={80} />}</h5>
                    <p className="product-card-brand">{'Realme' || <Skeleton width={60} />}</p>
                    <h4 className="product-card-price">{'product.price' ? '$456.00' : <Skeleton width={40} />}</h4>
                </div>
            </div>
        </SkeletonTheme>
    )
}
ProductItem.propType = {
    onClickItem: PropTypes.func,
    dispatch: PropTypes.func.isRequired,
    product: PropTypes.object.isRequired,
    onOpenModal: PropTypes.func,
    foundOnBasket: PropTypes.func.isRequired
};
export default ProductItem
